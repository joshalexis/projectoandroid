package com.project.efxcremote.database;

public abstract class DefineTable {
    public final static String TABLE_NAME = "presets";
    public final static String ID = "id";
    public final static String NOMBRE_PRESET = "nombre_preset";
    public final static String PEDAL_ONE = "pedal_one";
    public final static String PEDAL_TWO = "pedal_two";
    public final static String PEDAL_THREE = "pedal_three";
    public final static String PEDAL_FOUR = "pedal_four";
}
